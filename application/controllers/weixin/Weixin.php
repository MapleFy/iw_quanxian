<?php
class Weixin extends MY_HomeController
{
    function __construct()
    {
        parent::__construct();
        $this->load->library('lib_wechat');
        $this->load->model("user/m_aiw_dd","dd");
    }

    function msg_opt(){
        $wxobj = $this->lib_wechat;
        $type =  $wxobj->getRev()->getRevType();
        switch($type) {
            case Wechat::MSGTYPE_TEXT:
                if(trim($wxobj->getRev()->getRevContent())=="订餐"){
                    //$this->msg_send_dingcan();//回复图文
                }
                //$wxobj->text("hello, I'm wechat")->reply();
                exit;
                break;
            case Wechat::MSGTYPE_EVENT:
                break;
            case Wechat::MSGTYPE_IMAGE:
                break;
            default:
                $wxobj->text("help info")->reply();
        }
    }
    private function msg_send_dingcan()
    {
        $wxobj = $this->lib_wechat;
        $item = array(
            "0" => array(
                'Title' => '我要订餐',
                'Description' => '点击进入订餐系统',
                'PicUrl' => base_url() . 'static/theme/youbao/images/book_dinner_361.png',
                'Url' => base_url()
            )
        );
        //发送图文
        //file_put_contents("xxx.txt",print_r($wxobj->news($item)->reply()));
    }


    function valid()
    {
        $this->msg_opt();
        $this->lib_wechat->valid();
    }

    function cmenu()
    {
      
        $menu =  array (
            'button' => array (
                0 => array (
                    'name' => '档案',
                    'sub_button' => array (
                        0 => array (
                            'type' => 'view',
                            'name' => '局务公开',
                            'url' => 'http://www.baidu.com',
                        ),
                        1 => array (
                            'type' => 'view',
                            'name' => '业务指导',
                            'url' => 'http://www.baidu.com',
                        ),
                        2 => array (
                            'type' => 'view',
                            'name' => '业务咨询',
                            'url' => 'http://www.baidu.com',
                        ),
                        3 => array (
                            'type' => 'view',
                            'name' => '职称评审',
                            'url' => 'http://www.baidu.com',
                        ),
                        4 => array (
                            'type' => 'view',
                            'name' => '微首页',
                            'url' => 'http://www.baidu.com',
                        ),
                    ),
                ),
                1 => array (
                    'name' => '查档服务',
                    'sub_button' => array (
                        0 => array (
                            'type' => 'view',
                            'name' => '最新公告',
                            'url' => 'http://www.baidu.com',
                        ),
                        1 => array (
                            'type' => 'view',
                            'name' => '公开文件',
                            'url' => 'http://www.baidu.com',
                        ),
                        2 => array (
                            'type' => 'view',
                            'name' => '业务咨询',
                            'url' => 'http://www.baidu.com',
                        ),
                        3 => array (
                            'type' => 'view',
                            'name' => '档案搜索',
                            'url' => 'http://www.baidu.com',
                        ),
                        4 => array (
                            'type' => 'view',
                            'name' => '联系我们',
                            'url' => 'http://www.baidu.com',
                        ),
                    ),
                ),
                2 => array (
                    'name' => '方志地情',
                    'sub_button' => array (
                        0 => array (
                            'type' => 'view',
                            'name' => '中山概况',
                            'url' => 'http://www.baidu.com',
                        ),
                        1 => array (
                            'type' => 'view',
                            'name' => '中山年鉴',
                            'url' => 'http://www.baidu.com',
                        ),
                        2 => array (
                            'type' => 'view',
                            'name' => '中山志书',
                            'url' => 'http://www.baidu.com',
                        ),
                        3 => array (
                            'type' => 'view',
                            'name' => '统计数字',
                            'url' => 'http://www.baidu.com',
                        ),
                        4 => array (
                            'type' => 'view',
                            'name' => '中山大事',
                            'url' => 'http://www.baidu.com',
                        ),
                    ),
                ),
            ),
        );
        echo "<pre>";
        echo $this->lib_wechat->createMenu($menu)?"yes":(	$this->lib_wechat->errCode."|".$this->lib_wechat->errMsg);

        echo "<hr/>";
        print_r($menu);
        echo "</pre>";
    }
}